//
// Created by jens on 2015-05-22.
//
#include <iostream>
#include "tag-cloud-lib.hpp"

static unsigned numErrors = 0;

void test_toLowerCase();
void test_strip();

int main(int numArgs, char* args[]) {
    test_toLowerCase();
    test_strip();

    if (numErrors == 0) {
        cout << "All tests passed\n";
    }

    return 0;
}

void test_toLowerCase() {
    string result = toLowerCase("HippHappHoppABC");
    string expected = "hipphapphoppabc";
    if (result != expected) {
        ++numErrors;
        cout << "Test of toLowerCase failed: result=" << result << ", expected=" << expected << endl;
    }
}

void test_strip() {
    string payload = "abc hepp1234.!      tjena?";
    vector<string> expected = {"abc", "hepp", "tjena"};
    
    vector<string> result = split(payload, "[^a-zA-Z]+");
    if (result.size() != expected.size()) {
        ++numErrors;
        cout << "Test of strip(): different sizes. Expected=" << expected.size() 
             << ", but got=" << result.size() << endl;
        return;
    }

    const size_t N = expected.size();
    for (size_t k=0; k<N; ++k) {
        if (result[k] != expected[k]) {
            ++numErrors;
            cout << "Test of strip(): expected " << expected[k] << ", but got " << result[k] << endl;
            return;
        }
    }
}
