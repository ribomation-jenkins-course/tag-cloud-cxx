Tag Cloud
====

Sample C++ project that reads a text file and generates 
a HTML file with the 100 most frequent words of the input
and where each word is scaled proportional to its frequency.


Build
----

The build tool to use is CMake. Run the following commands

    mkdir ./build && cd $_
    cmake -G 'Unix Makefiles' ..
    make


Execution
----

    ./build/tag-cloud -in <input-file> -out <output-file> 

Additional options are

    --words <max-word-count>
    --size <min-word-size>
    --min-font <min-font-size>
    --max-font <max-font-size>
    -v   (verbose mode)


Sample Input File
----

Enclosed in the project is sample input file of size 5MB. The text file is
fetched from Project Gutenberg, which is a repo of non-copyright books,
and the file contains the collected work of William Shakespeare


