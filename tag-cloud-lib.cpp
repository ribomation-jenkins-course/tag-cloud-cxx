//
// Created by jens on 2015-05-22.
//

#include <iostream>
#include <algorithm>
#include <regex>
#include <random>

using namespace std;

#include "tag-cloud-lib.hpp"


string toLowerCase(const string& s) {
    string result{s};
    transform(s.begin(), s.end(), result.begin(), ::tolower);
    return result;
}

vector<string> split(const string& txt, const string& delimPattern) {
    const regex delimRE(delimPattern);
    sregex_token_iterator start(txt.begin(), txt.end(), delimRE, -1);
    sregex_token_iterator finish;

    vector<string> result{start, finish};
    return result;
}

WordFreqTable loadFrequencies(istream& input, unsigned int minWordSize) {
    WordFreqTable freqs;

    //read lines (or words)
    // skip non-letters
    // skip too small words
    // make into lower case
    // insert into freqs 

    for (string line; getline(input, line);) {
        vector<string> words = split(line, "[^a-zA-Z]+");
        for (auto word : words) {
            if (word.size() >= minWordSize) {
                freqs[toLowerCase(word)]++;
            }
        }
    }

    return freqs;
}

// process each word-freq pair and transform into word-size-color triplets
vector<WordTag> createTags(const vector<WordFreq>& freqs, unsigned int minFont, unsigned int maxFont) {
    const vector<string> colors = {
            "#ff0000", "#00ff00", "#0000ff",
            "#ffff00", "#00ffff", "#ff00ff"
    };
    uniform_int_distribution<size_t> colorIndex{0, colors.size() - 1};
    default_random_engine r;

    double scale = (double) (maxFont - minFont) / (freqs.front().second - freqs.back().second);

    vector<WordTag> tags;
    for (auto f : freqs) {
        string word = f.first;
        unsigned freq = f.second;
        unsigned fontSize = static_cast<unsigned>( minFont + scale * freq);
        string color = colors[colorIndex(r)];

        WordTag tag = make_tuple(word, fontSize, color);
        tags.push_back(tag);
    }

    return tags;
}

vector<string> createHtml(const vector<WordTag>& tags) {
    vector<string> spanTags;
    transform(tags.begin(), tags.end(), back_inserter(spanTags), [](WordTag t) {
        string word = get<0>(t);
        unsigned size = get<1>(t);
        string color = get<2>(t);
        string html = "<span style='font-size:" + to_string(size) + "px; " + "color:" + color + ";'>"
                      + word
                      + "</span>";
        return html;
    });
    return spanTags;
}

void emitHtml(const vector<string>& tags, ostream& output) {
    const string prefix = R"(
    <html>
    <head>
        <title>Word Cloud</title>
    </head>
    <body>
        <h1>Word Cloud</h1>
    )";
    const string suffix = R"(
    </body>
    </html>
    )";
    const string content = join(tags, "\n");

    output << prefix << content << suffix << endl;
}

string join(const vector<string>& payload, const string& delim) {
    string result = "";
    bool first = true;
    for (auto str : payload) {
        result += (first ? "" : delim) + str;
        first = false;
    }
    return result;
}


