#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <cstdlib>

using namespace std;

#include "tag-cloud-lib.hpp"


int main(int numArgs, char* args[]) {
    string inputFilename  = "shakespeare.txt";
    string outputFilename = "shakespeare.html";
    unsigned numWords = 100;
    unsigned minWordSize = 4;
    unsigned minFontSize = 10;
    unsigned maxFontSize = 150;
    bool verbose = false;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];

        if (arg == "-in") {
            inputFilename = args[++k];
        } else if (arg == "-out") {
            outputFilename = args[++k];
        } else if (arg == "-words") {
            numWords = (unsigned int) stoi(args[++k]);
        } else if (arg == "-size") {
            minWordSize = (unsigned int) stoi(args[++k]);
        } else if (arg == "-min-font") {
            minFontSize = (unsigned int) stoi(args[++k]);
        } else if (arg == "-max-font") {
            maxFontSize = (unsigned int) stoi(args[++k]);
        } else if (arg == "-v") {
            verbose = true;
        } else {
            cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << args[0] << " [-in file] [-out file] [-words int] [-size int] [-min-font int] [-max-font int] [-v]" << endl;
            return 1;
        }
    }


    // read file and create table of word-frequencies
    // skip too small words and also normalize
    ifstream input{inputFilename};
    if (!input) throw logic_error("cannot open " + inputFilename);

    WordFreqTable freqTbl = loadFrequencies(input, minWordSize);
    if (verbose) {
        cout << "--- freqTbl ---\n";
        for (auto item : freqTbl) {
            //cout << item.first << ": " << item.second << endl;
        }
    }

    // sort in desc frequency order
    vector<WordFreq> freqList{freqTbl.begin(), freqTbl.end()};
    sort(freqList.begin(), freqList.end(), [](const WordFreq& left, const WordFreq& right) {
        return right.second < left.second;
    });
    if (verbose) {
        cout << "--- freqList ---\n";
        for (auto item : freqList) {
            //cout << item.first << ": " << item.second << endl;
        }
    }

    // take the N most frequent words
    vector<WordFreq> mostFrequent;
    copy_n(freqList.begin(), numWords, back_inserter(mostFrequent));
    if (verbose) {
        cout << "--- most frequent ---\n";
        for (auto item : mostFrequent) {
            cout << item.first << ": " << item.second << endl;
        }
    }

    // process each word-freq pair and transform into word-size-color triplets
    vector<WordTag> tags = createTags(mostFrequent, minFontSize, maxFontSize);
    if (verbose) {
        cout << "--- tags data ---\n";
        for (auto tag : tags) {
            cout << get<0>(tag) << " | " << get<1>(tag) << " | " << get<2>(tag) << endl;
        }
    }

    // transform triplets into HTML SPAN tags
    vector<string> htmlSpan = createHtml(tags);
    if (verbose) {
        cout << "--- html span tags ---\n";
        for (auto span : htmlSpan) {
            cout << span << endl;
        }
    }

    // scramble the tag order 
    default_random_engine r;
    shuffle(htmlSpan.begin(), htmlSpan.end(), r);
    if (verbose) {
        cout << "--- html span tags in random order ---\n";
        for (auto span : htmlSpan) {
            cout << span << endl;
        }
    }

    // emit HTML document with the tags in place
    {
        ofstream output{outputFilename};
        if (!output) throw logic_error("cannot open " + outputFilename);
        emitHtml(htmlSpan, output);
    }

    return 0;
}
