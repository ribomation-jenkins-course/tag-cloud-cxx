//
// Created by jens on 2015-05-22.
//
#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <utility>
#include <tuple>
using namespace std;


typedef unordered_map<string, unsigned int> WordFreqTable;
typedef pair<string, unsigned int>  WordFreq;
typedef tuple<string, unsigned int, string> WordTag;


// Fordward Decls
WordFreqTable   loadFrequencies(istream&, unsigned int);
string          toLowerCase(const string&);
vector<WordTag> createTags(const vector<WordFreq>&freqs, unsigned int minFont, unsigned int maxFont);
vector<string>  createHtml(const vector<WordTag>& tags);
void            emitHtml(const vector<string>& tags, ostream& output);
vector<string>  split(const string& txt, const string& delimPattern);
string          join(const vector<string>& payload, const string& delim);

